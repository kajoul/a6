import java.util.*;

/**
 * Ülesanne:
 * Koostada meetod, mis arvutab etteantud sidusa lihtgraafi G=(V, E) iga tipu v jaoks välja selle ekstsentrilisuse:
 * e(v) = max {d(u, v)| u kuulub hulka V } , kus d(u, v) on tipu u kaugus tipust v (lühima tee pikkus tipust u tippu v)
 *
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   // User have to give number of vertices and number of edges to create random graph and
   // to find eccentricity for every vertex.
   // Number of edges can't be zero, less than nrOfVertices-1 and
   // bigger than nrOfVertices*(nrOfVertices-1)/2.
   // Graph has to have at least one vertex.
   int nrOfVertices = 6;
   int nrOfEdges = 9;

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {

      if(nrOfEdges < nrOfVertices-1 || nrOfEdges > nrOfVertices*(nrOfVertices-1)/2){
         throw  new IllegalArgumentException("Invalid number of edges: " + nrOfEdges);
      }
      if(nrOfVertices < 1){
         throw  new IllegalArgumentException("Invalid number of vertices: " + nrOfVertices);
      }

      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (nrOfVertices, nrOfEdges);
      System.out.println("Graph before finding eccentricities:");
      System.out.println (g);
      Vertex[] vertices = g.setEccentricities();
      System.out.println("Graph after finding eccentricities:");
      System.out.println (g);

      // Tests:
//      testSimple();
//      testWith2VerticesHaving2Edges();
//      testGraphWithLoops();
//      testWithMinimumNrOfVerticesAndEdges();
//      testWithLargeNrOfVerticesAndEdges();

   }

   /** Vertex represents vertex in the graph.
    * Every vertex has eccentricity.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int eccentricity = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      public Vertex getNextVertex(){
         return next;
      }

      public void setEccentricity(int e){
         this.eccentricity = e;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      private static final int INFINITY = Integer.MAX_VALUE / 2 ;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      /**
       * Converts adjacency matrix to the matrix of distances.
       * Inspiration from: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * @param mat adjacency matrix
       * @return matrix of distances, where each edge has length 1
       */
      public int[][] getDistMatrix(int[][] mat) {
         int nvert = mat.length;
         int[][] result = new int[nvert][nvert];
         if (nvert < 1) return result;

         for (int i=0; i<nvert; i++) {
            for (int j=0; j<nvert; j++) {
               if (mat[i][j] == 0) {
                  result[i][j] = INFINITY;
               } else {
                  result[i][j] = 1;
               }
            }
         }
         for (int i=0; i<nvert; i++) {
            result[i][i] = 0;
         }
         return result;
      }

      /**
       * Calculates shortest paths using Floyd-Warshall algorithm.
       * This matrix will contain shortest paths between each pair of vertices.
       * Inspiration from: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * @return matrix
       */
      public int[][] shortestPaths() {
         int[][] adjMatrix = createAdjMatrix();
         int[][] matrix = getDistMatrix(adjMatrix);
         int n = info; // number of vertices (createAdjMatrix() method counts vertices)
         if (n < 1) throw new RuntimeException("Count of vertices can't be lower than 1. Error in graph: " + id
         + " with " + info + " vertices.");

         for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
               for (int j = 0; j < n; j++) {
                  int newlength = matrix[i][k] + matrix[k][j];

                  if (matrix[i][j] > newlength) {
                     matrix[i][j] = newlength; // new path is shorter
                  }
               }
            }
         }
         return matrix;
      }

      /**
       * This method will add eccentricity for every vertex and returns array of Vertex.
       * @return array of Vertex
       */
      public Vertex[] setEccentricities() {
         int[][] matrix = shortestPaths();
         int[] eccentricities = new int[this.info];

         for (int i = 0; i < this.info; i++) {
            int eccentricity = 0;
            for (int j = 0; j < this.info; j++) {
               if (matrix[i][j] > eccentricity){
                  eccentricity = matrix[i][j];
               }
            }
            eccentricities[i] = eccentricity;
         }

         // maaran iga tipu v jaoks välja selle ekstsentrilisuse
         Vertex[] vertices = getAllVertices();
         int index = 0;
         for (Vertex vertex : vertices) {
            vertex.setEccentricity(eccentricities[index]);
            index++;
         }
         return vertices;
      }

      /**
       * This method will add all vertices of the graph to the array.
       * @return array of Vertex
       */
      public Vertex[] getAllVertices(){
         Vertex[] vertices = new Vertex[this.info];
         Vertex tp = first;
         int index = 0;
         while (tp != null) {
            vertices[index] = tp;
            tp = tp.getNextVertex();
            index++;
         }
         return vertices;
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" Eccentricity:" );
            sb.append (v.eccentricity);
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }


      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }
   }

   /**
    * Test for how many milliseconds it takes to find eccentricities of all the graph vertices,
    * when graph has 2200 vertices and 2200000 edges.
    */
   public void testWithLargeNrOfVerticesAndEdges() {
      Graph g_large = new Graph("G_Large");
      g_large.createRandomSimpleGraph(2200, 2200000);
      long startTime = System.currentTimeMillis();
      g_large.setEccentricities();
      long estimatedTime = System.currentTimeMillis() - startTime;
      System.out.println("It takes " + estimatedTime + " ms to find eccentricities of 2200 vertices");
   }

   /**
    * Test to find eccentricities of all the graph vertices, when graph has minimum nr of vertices and edges.
    * Graph has 1 vertex and 0 edges.
    */
   public void testWithMinimumNrOfVerticesAndEdges() {
      Graph test = new Graph("G_minimum");
      test.createRandomSimpleGraph(1, 0);
      System.out.println("Graph before finding eccentricities:");
      System.out.println(test.toString());
      test.setEccentricities();
      System.out.println("Graph after finding eccentricities:");
      System.out.println(test.toString());
      System.out.println("Graph with 1 vertex and 0 edges can't have any vertex eccentricities!");
      throw new IllegalArgumentException(test.id + " is a null graph.");
   }

   /**
    * Test to find eccentricities of all the graph vertices, when graph has 4 vertices.
    * 2 vertices have two edges and other 2 vertices have one edge.
    */
   public void testWith2VerticesHaving2Edges(){
      Graph test = new Graph("G");
      Vertex v1 = new Vertex("v1");
      Vertex v2 = new Vertex("v2");
      Vertex v3 = new Vertex("v3");
      Vertex v4 = new Vertex("v4");

      test.first = v1;
      v1.first = new Arc("v1-v2", v2, null);
      v1.next = v2;
      v2.first = new Arc("v2-v1", v1, new Arc("v2-v4", v4, null));
      v2.next = v3;
      v3.first = new Arc("v3-v4", v4, null);
      v3.next = v4;
      v4.first = new Arc("v4-v2", v2, new Arc("v4-v3", v3, null));

      System.out.println("Graph before finding eccentricities:");
      System.out.println(test.toString());
      Vertex[] vertices = test.setEccentricities();
      int[] actualEccentricities = getEccentricities(vertices);
      System.out.println("List of eccentricities was " + Arrays.toString(actualEccentricities));
      System.out.println("Expected list of eccentricities is [3, 2, 3, 2]");
      System.out.println(" ");
      System.out.println("Graph after finding eccentricities:");
      System.out.println(test.toString());
   }

   /**
    * Test to find eccentricities of all the graph vertices, when graph has 2 loops.
    */
   public void testGraphWithLoops(){
      Graph test = new Graph("G_loop");
      Vertex v1 = new Vertex("v1");
      Vertex v2 = new Vertex("v2");
      Vertex v3 = new Vertex("v3");
      Vertex v4 = new Vertex("v4");
      Vertex v5 = new Vertex("v5");

      test.first = v1;
      v1.first = new Arc("v1-v2", v2, new Arc("v1-v1", v1, null));
      v1.next = v2;
      v2.first = new Arc("v2-v1", v1, new Arc("v2-v5", v5, null));
      v2.next = v3;
      v3.first = new Arc("v3-v4", v4, new Arc("v3-v3", v3, null));
      v3.next = v4;
      v4.first = new Arc("v4-v5", v5, new Arc("v4-v3", v3, null));
      v4.next = v5;
      v5.first = new Arc("v5-v2", v2, new Arc("v5-v4", v4, null));

      System.out.println("Graph before finding eccentricities:");
      System.out.println(test.toString());
      Vertex[] vertices = test.setEccentricities();
      int[] actualEccentricities = getEccentricities(vertices);
      System.out.println("List of eccentricities was " + Arrays.toString(actualEccentricities));
      System.out.println("Expected list of eccentricities is [4, 3, 4, 3, 2]");
      System.out.println(" ");
      System.out.println("Graph after finding eccentricities:");
      System.out.println(test.toString());
   }

   /**
    * Test to find eccentricities of all the graph vertices.
    * Graph has 8 vertices.
    */
   public void testSimple() {
      Graph test = new Graph("G");
      Vertex v1 = new Vertex("v1");
      Vertex v2 = new Vertex("v2");
      Vertex v3 = new Vertex("v3");
      Vertex v4 = new Vertex("v4");
      Vertex v5 = new Vertex("v5");
      Vertex v6 = new Vertex("v6");
      Vertex v7 = new Vertex("v7");
      Vertex v8 = new Vertex("v8");

      test.first = v1;
      v1.first = new Arc("v1-v5", v5, new Arc("v1-v4", v4, null));
      v1.next = v2;
      v2.first = new Arc("v2-v7", v7, new Arc("v2-v8", v8, null));
      v2.next = v3;
      v3.first = new Arc("v3-v4", v4, null);
      v3.next = v4;
      v4.first = new Arc("v4-v7", v7, new Arc("v4-v1", v1, new Arc("v4-v3", v3, new Arc("v4-v8", v8, null))));
      v4.next = v5;
      v5.first = new Arc("v5-v8", v8, new Arc("v5-v1", v1, new Arc("v5-v7", v7, null)));
      v5.next = v6;
      v6.first = new Arc("v6-v7", v7, null);
      v6.next = v7;
      v7.first = new Arc("v7-v2", v2, new Arc("v7-v4", v4, new Arc("v7-v5", v5, new Arc("v7-v6", v6, new Arc("v7-v8", v8, null)))));
      v7.next = v8;
      v8.first = new Arc("v8-v5", v5, new Arc("v8-v2", v2, new Arc("v8-v4", v4, new Arc("v8-v7", v7, null))));

      System.out.println("Graph before finding eccentricities:");
      System.out.println(test.toString());
      Vertex[] vertices = test.setEccentricities();
      int[] actualEccentricities = getEccentricities(vertices);
      System.out.println("List of eccentricities was " + Arrays.toString(actualEccentricities));
      System.out.println("Expected list of eccentricities is [3, 3, 3, 2, 3, 3, 2, 2]");
      System.out.println(" ");
      System.out.println("Graph after finding eccentricities:");
      System.out.println(test.toString());
   }

   /**
    * Loops vertices and return array of vertex eccentricities.
    * @param vertices array of Vertex
    * @return array of ints, where ints are eccentricities
    */
   public int[] getEccentricities(Vertex[] vertices){
      int[] actualEccentricities = new int[vertices.length];
      int index = 0;
      for (Vertex v: vertices) {
         actualEccentricities[index] = v.eccentricity;
         index++;
      }
      return actualEccentricities;
   }
} 

